package com.iisysgroup.test;

public class Utils {

    public static final String RESULT_RECEIVER = "receiver";
    public static final String TRANSACTION_AMOUNT = "transaction_amount";
    public static final String TRANSACTION_ID = "transaction_id";
    public static final String TRANSACTION_ADDITIONAL_AMOUNT = "additional_amount";
    public static final String RECEIPT_DETAILS = "print_map";
    public static final String PACKAGE_NAME = "com.iisysgroup.androidlite";
    public static final String CLASS_DETAILS = "com.iisysgroup.androidlite.payments_menu.PurchaseActivity";
    public static final String CLASS_PRINT = "com.iisysgroup.androidlite.PrintActivity";
    public static final String CLASS_PREP = "com.iisysgroup.androidlite.TermMagmActivity";

    public static final int REQUESTCODE = 890;
    public static final String MAP  ="map";

    public static final String TRANSACTION_STATUS_REASON  ="transactionStatusReason";
    public static final String TRANSACTION_STATUS  ="transactionStatus";
    public static final String AMOUNT  ="amount";
    public static final String ADDITIONAL_AMOUNT ="ADDITIONAL AMOUNT";
    public static final String CARDHOLDER_NAME ="CARDHOLDER NAME";
    public static final String EXPIRY_DATE ="EXPIRY DATE";
    public static final String MID ="MID";
    public static final String AID ="AID";
    public static final String PAN ="PAN";
    public static final String LABEL ="LABEL";
    public static final String STAN ="STAN";
    public static final String RRN ="RRN";
    public static final String AUTH_ID ="AUTH ID";
    public static final String ACCOUNT_TYPE ="ACCOUNT TYPE";
    public static final String TERMINAL_ID ="TERMINAL ID";
    public static final String MERCHANT_NAME ="MERCHANT NAME";
    public static final String MERCHANT_ID ="MERCHANT ID";
    public static final String TRANSACTION_DATE = "DATE";
    public static final String TRANSACTION_TYPE = "TRANSACTION TYPE";
    public static final String TVR = "TVR";
    public static final String TSI = "TSI";
    public static final String RESPONSE_CODE = "RESPONSE CODE";

    public static final String PRINT_RECEIPT_VAS_TYPE = "vas_type";
    public static final String PURCHASE = "PURCHASE";

//    merchant name missing check previous code
//



}
