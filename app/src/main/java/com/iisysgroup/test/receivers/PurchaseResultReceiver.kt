package com.iisysgroup.test.receivers

import android.os.Bundle
import android.os.Handler
import android.os.ResultReceiver

class PurchaseResultReceiver(handler: Handler?) : ResultReceiver(handler) {
    override fun onReceiveResult(resultCode: Int, resultData: Bundle?) {
        super.onReceiveResult(resultCode, resultData)
    }
}