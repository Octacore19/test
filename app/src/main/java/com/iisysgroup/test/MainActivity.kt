package com.iisysgroup.test

import android.app.Activity
import android.content.ComponentName
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.iisysgroup.test.receivers.PurchaseResultReceiver
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    //These parameters are compulsory (required) to send to the printer
    private lateinit var transactionDate: String
    private lateinit var transactionAmount: String
    private lateinit var transactionType: String
    private lateinit var transactionReason: String
    private lateinit var transactionStatus: String

    //These parameters are optional
    private var additionalAmount: String? = null
    private var cardHolderName: String? = null
    private var cardExpiry: String? = null
    private var merchantID: String? = null
    private var authID: String? = null
    private var rrn: String? = null
    private var pan: String? = null
    private var label: String? = null
    private var responseCode: String? = null
    private var stan: String? = null
    private var aid: String? = null
    private var tvr: String? = null
    private var tsi: String? = null
    private var accountType: String? = null
    private var terminalID: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //Amount to be sent to NowNowApp for processing
        val amount = "2.00"

        button2.setOnClickListener{
            val intent = Intent()
            intent.putExtra("reprep", true)
            intent.component = ComponentName(Utils.PACKAGE_NAME, Utils.CLASS_PREP)
            if (packageManager.resolveActivity(intent, 0) != null) {
                startActivity(intent)
                Log.d("StartActivity", "Activity started successfully")
            } else {
                Toast.makeText(
                    this, "You Currently do not have an App to back this action", Toast.LENGTH_SHORT
                ).show()
            }
        }

        button.setOnClickListener {
            val intent = Intent()
            intent.putExtra(Utils.TRANSACTION_AMOUNT, amount)
            intent.putExtra(Utils.RESULT_RECEIVER, true)
            intent.component = ComponentName(Utils.PACKAGE_NAME, Utils.CLASS_DETAILS)

            //Call to trigger the payment on NowNowApp
            if (packageManager.resolveActivity(intent, 0) != null) {
                startActivityForResult(intent, Utils.REQUESTCODE)
                Log.d("StartActivity", "Activity started successfully")
            } else {
                Toast.makeText(
                    this, "You Currently do not have an App to back this action", Toast.LENGTH_SHORT
                ).show()
            }
        }

        //Call to trigger the printer on NowNowApp
        button3.setOnClickListener {
            val intent = Intent()
            val map = HashMap<String, String?>()
            map[Utils.ADDITIONAL_AMOUNT] = additionalAmount
            map[Utils.CARDHOLDER_NAME] = cardHolderName
            map[Utils.EXPIRY_DATE] = cardExpiry
            map[Utils.MERCHANT_ID] = merchantID
            map[Utils.AUTH_ID] = authID
            map[Utils.PAN] = pan
            map[Utils.LABEL] = label
            map[Utils.RESPONSE_CODE] = responseCode
            map[Utils.STAN] = stan
            map[Utils.RRN] = rrn
            map[Utils.AID] = aid
            map[Utils.TVR] = tvr
            map[Utils.TSI] = tsi
            map[Utils.ACCOUNT_TYPE] = accountType
            map[Utils.TERMINAL_ID] = terminalID

            intent.putExtra(Utils.PRINT_RECEIPT_VAS_TYPE, Utils.PURCHASE)
            intent.putExtra(Utils.TRANSACTION_DATE, transactionDate)
            intent.putExtra(Utils.TRANSACTION_TYPE, transactionType)
            intent.putExtra(Utils.TRANSACTION_STATUS, transactionStatus)
            intent.putExtra(Utils.MAP, map)
            intent.putExtra(Utils.AMOUNT, amount)
            intent.putExtra(Utils.TRANSACTION_STATUS_REASON, transactionReason)
            intent.putExtra(Utils.RESULT_RECEIVER, true)
            intent.component = ComponentName(Utils.PACKAGE_NAME, Utils.CLASS_PRINT)

            if (packageManager.resolveActivity(intent, 0) != null) {
                startActivity(intent)
                Log.d("PrintActivity", "Activity started successfully")
            } else {
                Toast.makeText(
                    this, "You Currently do not an App to back this action", Toast.LENGTH_SHORT
                ).show()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        Log.d("StartActivity", "Activity result received")
        if(requestCode == Utils.REQUESTCODE){
            if (resultCode == Activity.RESULT_OK){
                transactionDate = data!!.getStringExtra(Utils.TRANSACTION_DATE)!!
                transactionAmount = data.getStringExtra(Utils.AMOUNT)!!
                transactionType = data.getStringExtra(Utils.TRANSACTION_TYPE)!!
                transactionReason = data.getStringExtra(Utils.TRANSACTION_STATUS_REASON)!!
                transactionStatus = data.getStringExtra(Utils.TRANSACTION_STATUS)!!

                additionalAmount = data.getStringExtra(Utils.ADDITIONAL_AMOUNT)
                cardHolderName = data.getStringExtra(Utils.CARDHOLDER_NAME)
                cardExpiry = data.getStringExtra(Utils.EXPIRY_DATE)
                merchantID = data.getStringExtra(Utils.MERCHANT_ID)
                authID = data.getStringExtra(Utils.AUTH_ID)
                pan = data.getStringExtra(Utils.PAN)
                label = data.getStringExtra(Utils.LABEL)
                responseCode = data.getStringExtra(Utils.RESPONSE_CODE)
                stan = data.getStringExtra(Utils.STAN)
                rrn = data.getStringExtra(Utils.RRN)
                aid = data.getStringExtra(Utils.AID)
                tvr = data.getStringExtra(Utils.TVR)
                tsi = data.getStringExtra(Utils.TSI)
                accountType = data.getStringExtra(Utils.ACCOUNT_TYPE)
                terminalID = data.getStringExtra(Utils.TERMINAL_ID)

                //Displays the transaction result on the test application screen
                val response = "Date: $transactionDate\n" +
                        "Amount: $transactionAmount\n" +
                        "Transaction Type: $transactionType\n" +
                        "Transaction Status: $transactionStatus\n" +
                        "Transaction reason: $transactionReason\n" +
                        "Additional Amount: $additionalAmount\n" +
                        "Cardholder Name: $cardHolderName\n" +
                        "Card Expiry: $cardExpiry\n" +
                        "Merchant ID: $merchantID\n" +
                        "Auth ID: $authID\n" +
                        "PAN: $pan\n" +
                        "Label: $label\n" +
                        "Response Code: $responseCode\n" +
                        "STAN: $stan\n" +
                        "RRN: $rrn\n" +
                        "AID: $aid\n" +
                        "TVR: $tvr\n" +
                        "TSI: $tsi\n" +
                        "Account Type: $accountType\n" +
                        "Terminal ID: $terminalID"
                result.text = response
            }

            if (resultCode == Activity.RESULT_CANCELED){
                return
            }
        }
    }
}
